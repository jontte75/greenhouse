# Greenhouse Project #

This project contains source and fritzing of a project to automate a small greenhouse. For now, the soil-moisture probe is not "stable" enough, so automatic watering is not yet implemented. The current version does not contain a web-server, but sends data to thingspeak instead.

### Greenhouse repository ###
* source code for arduino microcontroller (pro mini)
* simplified fritzing picture of project.

### How do I get set up? ###
No special set-ups needed. Connect necessary devices and sensors to a arduino (project uses pro Mini clone, but any other should work just fine)

A simplified fritzing pic about the project:
![Project Frizing](greenhouse_bb.png)